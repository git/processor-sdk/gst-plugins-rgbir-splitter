Use  Processor SDK Linux for Edge AI v8.0.1 release (a.k.a. Edge AI SDK) as base package - this can be download from
https://www.ti.com/tool/download/PROCESSOR-SDK-LINUX-SK-TDA4VM

For building the plugin source code in Linux.

Go to the directory from terminal, where the source code is stored. A build folder will be created after executing 
these commands
	Run "CXXFLAGS=-ldl LDFLAGS=-Wl,--no-as-needed meson build"
	Run "ninja -C build"
	Run "cd build" (- i.e. enter the build the directory)
	Run "meson install"

NOTE: 	For running the pipelines successfully, libRGBIrIsPP.so file is needed in /usr/lib path. 
	After adding libRGBIrISPP.so file in /usr/lib path, run "ldconfig" command on the system being used.


Some Example pipelines are given below:

1 - 	GST_DEBUG=3 gst-launch-1.0 multifilesrc location=testImage1_1916x1076_P0_16b.bin blocksize=4123232 ! \
	video/x-raw,format=GRAY16_LE,width=1916,height=1076,framerate=1/1 ! rgbirsplitter name=demux demux.src_rgb ! \
	queue !  capssetter caps="video/x-bayer,format=(string)rggb,width=1916,height=1076" ! bayer2rgb ! \
	kmssink demux.src_ir ! queue ! fakesink 
	
	The above pipeline showcases how to read an RGBIR frame from file and displays the x-bayer rgb data using kmssink. 
	After reading the data using mutlifilesrc, the caps (or capabilities) for interpreting the data is specified using 
	a capsfilter. That data is then passed through rgbirsplitter element which splits the RGBIR data stream into x-bayer 
	RGB data stream and IR data stream. The x-bayer data is then converted to x-raw RGB using bayer2rgb element, for 
	displaying it using kmssink. The IR data is then passed to fakesink (simply discarded) in this pipeline. 

2 -	GST_DEBUG=3 gst-launch-1.0 multifilesrc location=testImage1_1916x1076_P0_16b.bin blocksize=4123232 ! \
	video/x-raw,format=GRAY16_LE,width=1916,height=1076,framerate=1/1 ! rgbirsplitter name=demux demux.src_rgb ! \
	queue ! fakesink demux.src_ir ! queue ! videoconvert ! kmssink
	
	The above pipeline showcases how to read an RGBIR frame from file and displays the IR data using kmssink. The 
	rgbirsplitter element splits data into two streams i.e x-bayer RGB and IR which is realised as x-raw GRAY8 format 
	which is then passed to kmssink through a videoconvert element. The x-bayer RGB data is passed to fakesink 
	(simply discarded) in this case.
	
3 -	GST_DEBUG=3 gst-launch-1.0 filesrc location=testImage1_1916x1076_P0_16b.bin blocksize=4123232 ! \
	video/x-raw, format=GRAY16_LE, width=1916, height=1076, framerate=1/1 ! rgbirsplitter name=demux demux.src_rgb ! \
	queue !  capssetter caps="video/x-bayer,format=(string)rggb,width=1916,height=1076" ! bayer2rgb ! jpegenc ! \
	filesink location=out_rgb.jpg demux.src_ir ! queue ! videoconvert ! jpegenc ! filesink location=out_ir.jpg
	
	This pipeline performs the similar function as mentioned in above pipelines, but instead of displaying the image 
	using kmssink, it stores the output in jpg files. 
	For viewing these jpg files using kmssink below pipeline can be used:
	"gst-launch-1.0 filesrc location=out1.jpg ! jpegdec ! imagefreeze ! videoconvert ! kmssink"
	

