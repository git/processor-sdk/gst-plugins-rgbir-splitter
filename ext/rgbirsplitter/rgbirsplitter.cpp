/*
 * Copyright (c) [2021] Texas Instruments Incorporated
 * 
 * All rights reserved not granted herein.
 * 
 * Limited License.  
 * 
 * Texas Instruments Incorporated grants a world-wide, royalty-free, 
 * non-exclusive license under copyrights and patents it now or hereafter 
 * owns or controls to make, have made, use, import, offer to sell and sell 
 * ("Utilize") this software subject to the terms herein.  With respect to 
 * the foregoing patent license, such license is granted  solely to the extent
 * that any such patent is necessary to Utilize the software alone. 
 * The patent license shall not apply to any combinations which include 
 * this software, other than combinations with devices manufactured by or
 * for TI (“TI Devices”).  No hardware patent is licensed hereunder.
 * 
 * Redistributions must preserve existing copyright notices and reproduce 
 * this license (including the above copyright notice and the disclaimer 
 * and (if applicable) source code license limitations below) in the 
 * documentation and/or other materials provided with the distribution
 * 
 * Redistribution and use in binary form, without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * *	No reverse engineering, decompilation, or disassembly of this software 
 *      is permitted with respect to any software provided in binary form.
 * 
 * *	Any redistribution and use are licensed by TI for use only with TI Devices.
 * 
 * *	Nothing shall obligate TI to provide you with source code for the
 *      software licensed and provided to you in object code.
 * 
 * If software source code is provided to you, modification and redistribution
 * of the source code are permitted provided that the following conditions are met:
 * 
 * *	Any redistribution and use of the source code, including any resulting 
 *      derivative works, are licensed by TI for use only with TI Devices.
 * 
 * *	Any redistribution and use of any object code compiled from the source
 *      code and any resulting derivative works, are licensed by TI for use 
 *      only with TI Devices.
 * 
 * Neither the name of Texas Instruments Incorporated nor the names of its 
 * suppliers may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER.
 * 
 * THIS SOFTWARE IS PROVIDED BY TI AND TI’S LICENSORS "AS IS" AND ANY EXPRESS 
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL TI AND TI’S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "config.h"
#include "rgbirsplitter.h"
#include <dlfcn.h>


static GstStateChangeReturn
rgbir_splitter_change_state (GstElement *element, GstStateChange transition);

static GstStateChangeReturn
openISPSharedLib (RgbirSplitter *self);

static gboolean rgbir_splitter_sink_event (GstPad    *pad,
                  GstObject *parent, GstEvent  *event);

static GstFlowReturn rgbir_splitter_chain (GstPad *pad, GstObject *parent,
    GstBuffer *inbuf_rgbir);

static gboolean rgbir_splitter_setcaps (RgbirSplitter *self,
               GstCaps *caps);

#define rgbir_splitter_parent_class parent_class
G_DEFINE_TYPE (RgbirSplitter, rgbir_splitter, GST_TYPE_ELEMENT);

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE (
  "sink",
  GST_PAD_SINK,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS ("video/x-raw, "
                   "  format = (string) GRAY16_LE, "
                   "  width = (int) [ 1, MAX ], " 
                   "  height = (int) [ 1, MAX ], "
                   "  framerate = (fraction) [ 0/1, 2147483647/1 ]")
);

static GstStaticPadTemplate src_template_rgb = GST_STATIC_PAD_TEMPLATE (
  "src_rgb",
  GST_PAD_SRC,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS ("video/x-bayer, "
                   "  format = (string) rggb, "
                   "  width = (int) [ 1, MAX ], "
                   "  height = (int) [ 1, MAX ], "
                   "  framerate = (fraction) [ 0/1, 2147483647/1 ]")
);

static GstStaticPadTemplate src_template_ir = GST_STATIC_PAD_TEMPLATE (
  "src_ir",
  GST_PAD_SRC,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS ("video/x-raw, "
                   "  format = (string) GRAY8, "
                   "  width = (int) [ 1, MAX ], "
                   "  height = (int) [ 1, MAX ], "
                   "  framerate = (fraction) [ 0/1, 2147483647/1 ]")
);

static void
rgbir_splitter_class_init (RgbirSplitterClass *klass)
{
  GstElementClass *eclass = GST_ELEMENT_CLASS (klass);

  gst_element_class_set_static_metadata (eclass,
      "RgbirSplitter",
      "Codec/Demuxer",
      "RGBIR splitter which splits RGBIR into raw RGB and IR",
      "Shubham Jain <s-jain1@ti.com>");

  gst_element_class_add_static_pad_template (eclass, &sink_template);
  gst_element_class_add_static_pad_template (eclass, &src_template_rgb);
  gst_element_class_add_static_pad_template (eclass, &src_template_ir);

  eclass->change_state = rgbir_splitter_change_state;

}

static void
rgbir_splitter_init (RgbirSplitter *self)
{
  GstElement *element = GST_ELEMENT (self);

  self->sinkpad = gst_pad_new_from_static_template (&sink_template, "sink");
  gst_pad_set_chain_function (self->sinkpad, rgbir_splitter_chain);
  gst_pad_set_event_function (self->sinkpad, rgbir_splitter_sink_event);
  gst_element_add_pad (element, self->sinkpad);

  self->srcpad_rgb = gst_pad_new_from_static_template (&src_template_rgb, 
                                                        "src_rgb");
  gst_element_add_pad (element, self->srcpad_rgb);

  self->srcpad_ir = gst_pad_new_from_static_template (&src_template_ir, 
                                                      "src_ir");
  gst_element_add_pad (element, self->srcpad_ir);

}

static GstStateChangeReturn
rgbir_splitter_change_state (GstElement *element, GstStateChange transition)
{
  RgbirSplitter *self;
  GstStateChangeReturn ret;

  self = RGBIR_SPLITTER (element);

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      ret = openISPSharedLib(self);
      if(!ret)
        return ret;
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY: 
      delete self->ip_params;
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      break;
    default:
      break;
  }

  return ret;
}

static GstStateChangeReturn
openISPSharedLib (RgbirSplitter *self)
{
  int (*setDefaultParamsFunc)(RGBIRISPPLIBParams *);
  char *error;
  void *handle;
  handle = dlopen("libRGBIrISPP.so", RTLD_LAZY);
      
  if (!(handle)) {
    fprintf(stderr, "%s\n", dlerror());
    return GST_STATE_CHANGE_FAILURE;
  }
      
  dlerror();
  self->ip_params = new RGBIRISPPLIBParams;
  *(void **) (&setDefaultParamsFunc) = dlsym(handle, 
                                        "rgbirISPPSetDefaultParameters");

  error = dlerror();
  if (error != NULL) {
    fprintf(stderr, "%s\n", error);
    return GST_STATE_CHANGE_FAILURE;
  }

  (*setDefaultParamsFunc)(self->ip_params);

  *(void **) (&(self->rgbirSplitFunc)) = dlsym(handle, 
                                              "rgbirISPPProcess");

  error = dlerror();
  if (error != NULL) {
    fprintf(stderr, "%s\n", error);
    return GST_STATE_CHANGE_FAILURE;
  }

  dlclose (handle);

  return GST_STATE_CHANGE_SUCCESS;
}

static gboolean
rgbir_splitter_sink_event (GstPad    *pad,
                  GstObject *parent,
                  GstEvent  *event)
{
  gboolean ret;
  RgbirSplitter *self = RGBIR_SPLITTER (parent);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CAPS:
      GstCaps *caps;
      gst_event_parse_caps (event, &caps);
      ret = rgbir_splitter_setcaps (self, caps);
      break;
    default:
      ret = gst_pad_event_default (pad, parent, event);
      break;
  }

  return ret;
}

static gboolean
rgbir_splitter_setcaps (RgbirSplitter *self,
               GstCaps *caps_sink)
{
  GstStructure *s;
  gint fr_n, fr_d;
  GstCaps *caps_rgb, *caps_ir;

  s = gst_caps_get_structure (caps_sink, 0);

  if (!gst_structure_get_int (s, "width", &(self->width)) ||
      !gst_structure_get_int (s, "height", &(self->height)) ||
      !gst_structure_get_fraction (s, "framerate", &fr_n, &fr_d)) {
    GST_ERROR_OBJECT (self, "Incomplete caps for sink pad");
    return FALSE;
  }

  caps_rgb = gst_caps_new_simple("video/x-bayer",
                                   "width", G_TYPE_INT, self->width,    
                                   "height", G_TYPE_INT, self->height,
                                   "framerate", GST_TYPE_FRACTION, fr_n, fr_d,  
                                   "format", G_TYPE_STRING, "rggb",
                                   NULL);

  if (!gst_pad_set_caps (self->srcpad_rgb, caps_rgb)) {
      GST_ELEMENT_ERROR (self, CORE, NEGOTIATION, (NULL),
            ("Unable to set caps over srcpad_rgb"));
      return FALSE;
  }

  caps_ir = gst_caps_new_simple("video/x-raw",
                                   "width", G_TYPE_INT, self->width,
                                   "height", G_TYPE_INT, self->height,
                                   "framerate", GST_TYPE_FRACTION, fr_n, fr_d,
                                   "format", G_TYPE_STRING, "GRAY8",
                                   NULL);

  if (!gst_pad_set_caps (self->srcpad_ir, caps_ir)) {
      GST_ELEMENT_ERROR (self, CORE, NEGOTIATION, (NULL),
          ("Unable to set caps over srcpad_ir"));
      return FALSE;
  }

  gst_caps_unref (caps_rgb);
  gst_caps_unref (caps_ir);

  return TRUE;
}

static GstFlowReturn
rgbir_splitter_chain (GstPad *pad, GstObject *parent, GstBuffer *inbuf_rgbir)
{
  RgbirSplitter *self;
  GstFlowReturn ret;
  GstMapInfo inmap_rgbir, outmap_rgb, outmap_ir;
  GstBuffer *outbuf_rgb, *outbuf_ir;

  self = RGBIR_SPLITTER (parent);

  if (!gst_buffer_map (inbuf_rgbir, &inmap_rgbir, GST_MAP_READ)) {
    gst_buffer_unref (inbuf_rgbir);
    GST_ELEMENT_ERROR (self, STREAM, DECODE,
                       ("Unable to map buffer"),
                       (NULL));
    return GST_FLOW_ERROR;
  }

  outbuf_rgb = gst_buffer_new_allocate(NULL, inmap_rgbir.size / 2 , NULL);
  outbuf_ir = gst_buffer_new_allocate(NULL, inmap_rgbir.size / 2, NULL);

  if (!gst_buffer_map (outbuf_rgb, &outmap_rgb, GST_MAP_WRITE) || 
      !gst_buffer_map (outbuf_ir, &outmap_ir, GST_MAP_WRITE)) {
    gst_buffer_unmap (inbuf_rgbir, &inmap_rgbir);
    gst_buffer_unref (inbuf_rgbir);
    gst_buffer_unref (outbuf_rgb);
    GST_ELEMENT_ERROR (self, STREAM, DECODE,
                       ("Unable to map output buffer"),
                       (NULL));
    return GST_FLOW_ERROR;
  }
  
  /* 
  Set irRemapLUT=NULL if no LUT is provided from App.
  Set outputFormat=0 for 16-bpp output, set outputFormat=1 for 8-bpp output
  */
  (*(self->rgbirSplitFunc))(self->ip_params, self->height, self->width, 
                            (unsigned short *)inmap_rgbir.data, 
                            (unsigned short *)outmap_rgb.data, 
                            (unsigned short *)outmap_ir.data, NULL, 1);  

  gst_buffer_unmap (outbuf_rgb, &outmap_rgb);
  gst_buffer_unmap (outbuf_ir, &outmap_ir);
  gst_buffer_unmap (inbuf_rgbir, &inmap_rgbir);
  gst_buffer_unref (inbuf_rgbir);

  ret = gst_pad_push (self->srcpad_rgb, outbuf_rgb);
  if(ret != GST_FLOW_OK)
    return ret;

  ret = gst_pad_push (self->srcpad_ir, outbuf_ir);
  return ret;
}

