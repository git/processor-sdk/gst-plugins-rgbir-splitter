/**
 * TI C++ Reference software for Computer Vision Algorithms (TICV)
 * TICV is a software module developed to model computer vision
 * and imaging algorithms on TI's various platforms/SOCs.
 *
 * Copyright (C) 2021 Texas Instruments Incorporated - http://www.ti.com/
 * ALL RIGHTS RESERVED
 */

/**
 * @file:       libRGBIrISPP.h
 * @brief:      APIs to test TI 4x4 RGBIR CFA preprocessing HWA functionality.
 */

#ifndef LIB_RGBIR_ISPP_H
#define LIB_RGBIR_ISPP_H

#include <stdio.h>
#include <stdlib.h>

typedef struct RGBIRISPPLIBParams {
    /*
     *  Input CFA format, 0 = RGGIr(default), 1 = GRIrG, 2 = BGGIr, 3 = GBIrG, 4 = GIrRG, 5 = IrGGR , 6 = GIrBG, 7= IrGGB 
     */
    int inputCfaFormat;
    
    /*
     *  Enable output on the bayer channel
     *  0 = Disable
     *  1 = Enable (default)
     */
    int bayerOutEn;
    
    /*
     *  Enable ir subtraction for the bayer output
     *  0 = Disable
     *  1 = Enable (default)
     */
    int irsubEn;
    
    /*
     *  Enable output on the ir channel
     *  0 = Disable
     *  1 = Enable (default)     
     */
    int irOutEn;
    
    /*
     *  Control to select the R/B/IR interpolation method 
     *  0:  Constant hue interpolation (default)
     *  1:  Color difference interpolation
     */
    int pcidRBIrInterpMethod;

    /*
     *  Three thresholds used for interpolation predictors selection during interpolation of R, B and Ir channels
     *  For 16 raw data following values can be used (default)
     *            pcidt1 = 8192 
     *            pcidt2 = 16320
     *            pcidt3 = 32768
     */
    int pcidt1;
    int pcidt2;
    int pcidt3;

    /*
     *  Parameter to control the portion of green high frequency component added to interpolated R/B and Ir 
     *  pixels when using color difference interpolation. The values should be a real number in range [0, 1]
     *  provided in U9Q8 fixed point format. Default value is 128.
     */
    int pcidGHFXferScale;
    int pcidGHFXferScaleIr;

    /*
     *  The type of pixel (R/B) value to be interpolated at Ir location in the input raw image for 
     *  pattern conversion into bayer image. 
     *  0:  B channel values are interpolated at input Ir location R channel values are interpolated at input B locations (default)
     *  1:  R channel values are interpolated at input Ir location B channel values are interpolated at input R locations 
     */
    int pcidRBInterpPreference;
    
    /*
     *  Parameter to control which of the bayer/Ir output is processed by downstream VISS pipeline such as noise filter, tonemapping etc.
     *  0 = Bayer output of the PCID block goes to downstream VISS pipeline (default)
     *  1 = Ir output of the PCID block goes to downstream VISS pipeline
     */
    int pcidBayerIrOpToDownstreamVISS;

    /*
     *  The value of a pixel(R/G/B) above which Ir subtraction is disabled.
     *  Default value is 65534
     */
    int irsubCutoffTh;

    /*
     *  Parameter to control spatial filtering of the ir subtraction factors 
     *  0 = No spatial filtering
     *  1 = 5x5 low averaging filter is applied on the calcualted ir subtraction factors before using for ir subtraction from bayer output (default)
     */
    int irsubType;
    
    /*
     *  The parameter is used to define the transition region of [irsubCutoffThreshold-irsubTransitionBandWidth, irsubCutoffThreshold] 
     *  over which the portion of Ir subtracted transitions from 100% (at R/G/B <= irsubCutoffThreshold-irsubTransitionBandWidth) to 
     *  0% (at R/G/B >= irsubCutoffThreshold). Default value is 5.
     */
    int irsubTransBW;

    /*
     *  The parameter is reciprocal of irsubTransBW in U9Q8 format. Default value is 51.
     */
    int irsubTBWReciprocal;

    /*
     *  This parameter defines the scale (real value in range [0, 1]) applied to the dynamically 
     *  calculated Ir subtraction factor. Represented in U9Q8 format.
     */
    int irsubScale;

    /*
     *  The parameter defines 5 scale factors (real value in range [0, 1]) applied to the Ir subtraction 
     *  factor that is calculated based on the maximum R/G/B value in the neighbourhood of a pixel.
     *  This scale factors depend on pixel under consideration and its distance from the R/G/B pixel 
     *  having maxim value in the neighbourhood. TH epixel distance are quantized in 5 values. 
     *  Parameter is represented in U9Q8 format. Default values {256, 256, 192, 128, 64}. 
     */

    int irsubMaxRGBDistFactorLUT[5];

    /*
     *  Option to allow gray level remapping of the Ir output from PCID block. 
     *  0 = Disabled (default)
     *  1 = Enabled
     */
    int irRemapEn;
}RGBIRISPPLIBParams;


#ifdef __cplusplus
 #define EXTERNC extern "C"
#else
 #define EXTERNC
#endif

    /**
     *  Interface for integration of TI RGBIR Pattern Conversion and Ir Upsampling block functionality
     *    @in    :    inputParams        : Configuration parameters in form of a RGBIRISPPLIBParams structure. 
     *                inputHeight        : Input raw image width in pixels
     *                inputWidth         : Input raw image height in pixels
     *                rawInput           : Base pointer for uint16 buffer containing input raw image. Line pitch for the buffer must be equal to inputWidth
     *                irRemapLUT         : Base pointer for uint16 buffer containing IR remapping LUT.
     *
     *                outputFormat       : [IMPORTANT] Must be set to 0 unless specified otherwise.
     * 
     *    @out:       bayerOutput        : Base pointer for uint16 buffer to return pattern conveerted bayer image
     *                irOutput           : Base pointer for uint16 buffer to return upsampled Ir image
     *
     *  @Future support 
     *            MMR Configurations
     */
EXTERNC int rgbirISPPProcess(RGBIRISPPLIBParams *inputParams, int inputHeight, int inputWidth, unsigned short *rawInput, unsigned short *bayerOutput, unsigned short*irOutput, int *irRemapLUT, int outputFormat);

    /**
     *  Function to otain default values for RGBIR ISPP simulator 
     *     @out: params        : RGBIRISPPLIBParams structure is populated with the default values.
     *            
     */

EXTERNC int rgbirISPPSetDefaultParameters(RGBIRISPPLIBParams* params);

#undef EXTERNC

#endif /* LIB_RGBIR_ISPP_H */
